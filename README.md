[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Release](https://img.shields.io/badge/Release-v1.3.0-brightgreen)](https://gitea.com/6543/gitea_sortt/releases)


# gitea_sortt

Java Script for https://github.com/go-gitea/gitea/pull/7980 (Sortable Tables) .. by header click

sortt( [normsort], (revsort), (isdefault) )

## Routine Idear
1. add to table head colum onclick="sortt('sort parameters')"
2. Klich at colum header for example `<ID>`
  * script check if url has already a sort= with `<ID>`
     - NO: if sort="" indikates default (with * prefix) set URLsort=<default>
     - YES: set URLsort variable
  * script check if URLsort and sort attribute is same
     - YES: check if reverse attribute exist
        -> YES: generate URL with reverse sort param and open
     - NO: generate URL with sort param and open
