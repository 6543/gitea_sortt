/*
  Sortt

  @License MIT
  @Author 6543
  @Repository https://gitea.com/6543/gitea_sortt
  @Version 1.3.0

*/

//Declare Functions

//create global function with main routine
window.sortt=function(normsort,revsort,isdefault){
  //sortt [normsort] (revsort) (isdefault)

  //normsort is needed
  if (!(normsort)) return false;

  //default values of optinal parameters
  if (!(revsort)) revsort = "";

  // parse URL
  /*  script check if url has already a sort=
        NO: if normsort is default set url_sort=
					YES: set url_sort variable
  */
  let url = new URL(window.location);
  let url_sort = url.searchParams.get("sort");
  if ((url_sort === null) && isdefault) url_sort = normsort;

  // generate new URL
  /*  script check if url_sort and sort attribute is same
        NO: generate URL with sort param
        YES: check if reverse attribute exist -> YES: generate URL with reverse sort param
  */
  if (url_sort != normsort) {
    url.searchParams.set("sort",normsort);
  } else if (revsort != "") {
    url.searchParams.set("sort",revsort);
  }

  //open url
  window.location.replace(url.href);
};

//create global function with main routine
function getArrow(normsort,revsort,isdefault){
	//sortt [normsort] (revsort) (isdefault)

	//arrows
	var arrow_down = '⯆';  // U+2BC6
	var arrow_up = '⯅';		 // U+2BC5

  //normsort is needed
  if (!(normsort)) return false;

  //default values of optinal parameters
  if (!(revsort)) revsort = "";

	//get sort param from url
  let url_sort = (new URL(window.location)).searchParams.get("sort");

  if ((url_sort === null) && isdefault) {
		//if sort is sorted as default add arrow tho this table header
		if (isdefault) return arrow_down;
	} else {
		//if sort arg is in url test if it corelates with colum header sort arguments
		if (url_sort === normsort) {
			//the table is sorted with this header normal
			return arrow_down;
		} else if (url_sort === revsort) {
			//the table is sorted with this header reverse
			return arrow_up;
		} else {
			//the table is NOT sorted with this header
			return false;
		}
	}
}


//USE Functions

// test if jQuery is available
if(!window.jQuery) {
	console.log("sortt.js: ERROR no jQuery found!");
} else {

  //use JQuery to go throu each table header with "data-sortt" attribute
  $('th').each(function() {
		//if data attribute sortt is set
    if ($(this)[0].dataset.sortt) {
			//get data
			var data = $(this)[0].dataset.sortt;
			data = data.split(",");

			//add onclick event
			$(this).on('click', function() {
		  	sortt(data[0],data[1],data[2]);
    	});

			//add arrow to colume
			var arrow = getArrow(data[0],data[1],data[2]);
			// if function got a match ...
			if (arrow != false ) {
				$(this).prepend(arrow + " ");
			}
		}
  });

}
